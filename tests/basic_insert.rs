extern crate rand;

use voxer::{
    *,
    octree::{
        SVO,
        FlattenTree,
        ToFlatVoxel,
        OctNode,
    },
    hash_acc::HashAcc,
    Tooling
};

#[derive(Clone)]
struct Voxel(f32);

impl Tooling for Voxel{
    
    fn empty() -> Self {
        Voxel(0.0)
    }
    
    fn interpolate(&mut self, weight_self: f32, weights: &[(f32, Self)]) {
        let  new = weight_self * self.0;
        let children = weights.iter().map(|(w, v)| w * v.0).fold(0.0, |old, new| old+new);
        self.0 = new + children;
    }
}

struct FlatVoxel(f32, usize);

impl ToFlatVoxel<Voxel, FlatVoxel> for Voxel{
    fn flatten(&self, skip_index: usize, level: usize, node: &OctNode<Voxel>) -> FlatVoxel{
        FlatVoxel(self.0, skip_index)
    } 
}


const RES: u32 = 4096;
const TEST_SIZE: u32 = 100_000;

fn get_inserts() -> Vec<(f32, [u32;3])>{
    (0..TEST_SIZE).into_iter().map(|x| (
        x as f32,
        [
            (rand::random::<f32>() * RES as f32).floor() as u32,
            (rand::random::<f32>() * RES as f32).floor() as u32,
            (rand::random::<f32>() * RES as f32).floor() as u32,
        ]
    )).collect()
}

fn insert(inserts: &[(f32, [u32;3])], obj: &mut Object<Voxel, impl Accelerator<Voxel>>){
    for (f, l) in inserts.iter(){
        obj.insert(*l, Voxel(*f));
    }
}

fn insert_and_get(mut object: Object<Voxel, impl Accelerator<Voxel>>){
       
    let inserts = get_inserts();

    insert(&inserts, &mut object);

    for (e,(_f,l)) in inserts.iter().enumerate(){
        assert!(object.get(l).is_some(), "{:?} should be some in iter {}", l, e);
    }
}

#[test]
fn insert_get_svo(){
    let object: Object<Voxel, SVO<Voxel>> = Object::new(SVO::new(None));
    insert_and_get(object);
}


#[test]
fn insert_get_hash(){
    let object: Object<Voxel, HashAcc> = Object::new(HashAcc::new());
    insert_and_get(object);
}

fn delete(object: &mut Object<Voxel, impl Accelerator<Voxel>>){
    let inserts = get_inserts();

    insert(&inserts, object);

    for (e, (_f, l)) in inserts.iter().enumerate(){
        if e % 3 == 0{
            object.delete(l);
        }
    }

    for (e, (_f, l)) in inserts.iter().enumerate(){
        if e% 3 == 0{
            assert!(object.get(&l).is_none(), "{:?} should have been none, but was some", l);
        }
    }
}

#[test]
fn delete_svo(){
    let mut object: Object<Voxel, SVO<Voxel>> = Object::new(SVO::new(None));
    delete(&mut object);
}


#[test]
fn delete_hash(){
    let mut object: Object<Voxel, HashAcc> = Object::new(HashAcc::new());
    delete(&mut object);
}

#[test]
fn flatten(){
    let mut object: Object<Voxel, SVO<Voxel>> = Object::new(SVO::new(None));
    insert(&get_inserts(), &mut object);

    let flat_buffer = object.to_flat_tree();
}

#[test]
fn iterator(){
    let mut object: Object<Voxel, SVO<Voxel>> = Object::new(SVO::new(None));
    insert(&get_inserts(), &mut object);

    let location: Vec<_> = object.iter().map(|(loc, _vox)| loc).collect();
    assert!(location.len() == TEST_SIZE as usize, "Number of location should be TEST_SIZE"); 
}
