extern crate rand;

use voxer::{
    *,
    octree::SVO,
};

#[derive(Clone)]
struct Voxel(f32);

impl Tooling for Voxel{
    
    fn empty() -> Self {
        Voxel(0.0)
    }
    
    fn interpolate(&mut self, weight_self: f32, weights: &[(f32, Self)]) {
        let  new = weight_self * self.0;
        let children = weights.iter().map(|(w, v)| w * v.0).fold(0.0, |old, new| old+new);
        self.0 = new + children;
    }
}

fn sleep(){
    std::thread::sleep(std::time::Duration::from_millis(100));
}

const NUM_VOXEL: usize = 73__000;
fn insert_and_get(){
    
    let res = 4096;
    let mut obj: Object<Voxel, _> = Object::new(SVO::new(None));
    
    let inserts: Vec<(f32, [u32; 3])> = (0..NUM_VOXEL).into_iter().map(|x| (
        x as f32,
        [
            (rand::random::<f32>() * res as f32).floor() as u32,
            (rand::random::<f32>() * res as f32).floor() as u32,
            (rand::random::<f32>() * res as f32).floor() as u32,
        ]
    )).collect();


    for (f, l) in inserts.iter(){
        obj.insert(*l, Voxel(*f));
    }

    for (e,(_f,l)) in inserts.iter().enumerate(){
        assert!(obj.get(l).is_some(), "{:?} should be some in iter {}", l, e);
    }
}

pub fn main(){

    let mut object: Object<Voxel, _> = Object::new(SVO::new(None));

    object.insert([6740, 7849, 2351], Voxel(1.0));

    sleep();

    object.insert([63,12,42], Voxel(25.0));

    sleep();

    object.get_accelerator().print_all_nodes();
    
    assert!(object.get(&[6740, 7849, 2351]).is_some(), "Was 1,1,1 None");
    println!("---");
    assert!(object.get(&[63,12,42]).is_some(), "Was 63,12,42 None");
    let inst = std::time::Instant::now();
    insert_and_get();
    println!("Inserting {} was: {}ms or {}s", NUM_VOXEL, inst.elapsed().subsec_millis(), inst.elapsed().as_secs_f32());
    println!("is {}ms per ", inst.elapsed().as_secs_f32() * 1_000.0 / NUM_VOXEL as f32);
    println!("is {}ns per ", inst.elapsed().as_secs_f32() * 1_000_000.0 / NUM_VOXEL as f32);
}
