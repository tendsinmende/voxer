
/*!
# Basic structure:
The library is able to load and store voxel volumes from files via the `Encode` and `Decode` traits.
Those objects are then stored in a linear `VoxelBuffer`. The buffer itself only stores the voxels but has no knowledge 
where in the volume the voxels are located. This is handled by the associated `Accelerator`.

The library is Voxel-agnostic. You can store anything as a voxel. However, if you want some further functionality like
bluring the volume, the stored object needs to implement the `Tooling` trait and your voxel must be `Clone`. See the example
below on how to implement this for a very basic voxel.

An Accelerator handles indexing the buffer when trying to receive a voxel at a location. The two most common types are
a HashMap (implemented as `HashAcc`) and an Sparse-Voxel-Octree (implemented as `SVO`). But you can also implement
new accelerators via the `Accelerator` trait.

# Important notes
## Inserting out of bound
When inserting a voxel that is not within the current bounds of the volume, the volumen will be
grown until it fits the voxel.

In a Hash Accelerator this is a simple insert, however in an octree this might create some overhead.
For instance if all voxels fitted in a 32³ volume, the resolution for the whole volume is 32³. If you add a voxel at [8100, 511, 32]
the volume grows to 8192³ and create several new sub voxels to contain the first 32³ voxel as well as the new "out-of-bounds" voxel.

## Removing
When a voxel is removed from an object, the accelerator will remove its reference into the buffer and the buffer marks it for overwriting.
However, it is not deleted from memory. At some point there might be a housekeeping function that updates the buffer and accelerator when 
the fragmentation is to big. However, this is not yet implemented. 

# Usage
Creating a simple object and inserting a voxel:
```
use voxer::{
    Object,
    SVO,
    Tooling
};

#[derive(Clone)]
struct Voxel(f32);
///Example Tooling implementation needed to use the SVO.
///Otherwise we could only use the standard HashAcc as our accelerator.
impl Tooling for Voxel{
    fn empty() -> Self {
        Voxel(0.0)
    }    
    fn interpolate(&mut self, weight_self: f32, weights: &[(f32, Self)]) {
        let  new = weight_self * self.0;
        let children = weights.iter().map(|(w, v)| w * v.0).fold(0.0, |old, new| old+new);
        self.0 = new + children;
    }
}

fn main(){
    let mut object: Object<Voxel, _> = Object::new(SVO::new(None));
    object.insert([32,56,7], Voxel(25.0));
}
```

!*/

///Everything related to the Hashmap based acceleration.
pub mod hash_acc;
pub use hash_acc::HashAcc;
///Everything related to the Octree acceleration structure.
pub mod octree;
pub use octree::{
    SVO,
    FlattenTree,
    FlattenNode,
    ToFlatVoxel
};
///Abstract encode trait 

///Indexing primitive used when accessing a voxel within a VoxelBuffer.
pub type VoxIndex = usize;

///A Linear buffer containing the raw voxels of an object
pub struct VoxelBuffer<V>{
    buffer: Vec<V>,
    deleted_voxels: Vec<VoxIndex>,
}

impl<V> VoxelBuffer<V>{
    ///Inserts a voxel and returns the index at which it was added.
    ///This is not necessarily the end of this buffer.
    pub fn push(&mut self, voxel: V) -> VoxIndex{
        if let Some(i) = self.deleted_voxels.pop(){
            //Overwrite at location
            self.buffer[i] = voxel;
            i
        }else{
            let i = self.buffer.len();
            self.buffer.push(voxel);
            i
        }
    }

    ///Exchanges a voxel at the given index with the new one. Returns the old voxel.
    /// Does nothing and returns `None` if there is no voxel at this index.
    pub fn exchange(&mut self, mut new_voxel: V, index: VoxIndex) -> Option<V>{
        if let Some(vox) = self.buffer.get_mut(index){
            std::mem::swap(vox, &mut new_voxel)
        }else{
            return None;
        }
        Some(new_voxel)
    }

    ///Does not actually deletes a voxel, but marks it for replacement.
    ///Note, after deleting a voxel, the voxel that was at this index can be overwritten at any push to this buffer.
    pub fn delete(&mut self, index: VoxIndex){
        self.deleted_voxels.push(index);
    }
    
    pub fn get(&self, index: VoxIndex) -> Option<&V>{
        self.buffer.get(index)
    }
    
    pub fn get_mut(&mut self, index: VoxIndex) -> Option<&mut V>{
        self.buffer.get_mut(index)
    }
}

///Abstract voxel object created with an Accelerator `A` for voxels of type `V.
///
/// The object can be loaded from a file via a the `Decode` trait and stored as well via `Encode`.
/// By default these traits are not implemented on anything.
///
/// There is a shortcut function for creating an empty, Hashmap accelerated object via `simple_object()`. 
pub struct Object<V, A> where A: Accelerator<V>{
    buffer: VoxelBuffer<V>,
    accelerator: A,
}

impl<V, A> Object<V, A> where A: Accelerator<V>{
    pub fn new(accelerator: A) -> Self{
        Object{
            buffer: VoxelBuffer{
                buffer: Vec::new(),
                deleted_voxels: Vec::new(),
            },
            accelerator: accelerator
        }
    }

    pub fn insert(&mut self, location: [u32;3], voxel: V){
        let index = self.buffer.push(voxel);
        self.accelerator.insert(location, index, &mut self.buffer);
    }
    
    pub fn delete(&mut self, location: &[u32;3]){
        if let Some(index) = self.accelerator.get(location){
            self.buffer.delete(*index);
            self.accelerator.delete(location, &mut self.buffer);
        }
    }

    pub fn exchange(&mut self, location: &[u32;3], voxel: V) -> Option<V>{
        if let Some(index) = self.accelerator.get(location){
            self.buffer.exchange(voxel, *index)
        }else{
            None
        }
    }
    
    pub fn get(&self, location: &[u32; 3]) -> Option<&V>{
        if let Some(index) = self.accelerator.get(location){
            self.buffer.get(*index)
        }else{
            None
        }
    }
    pub fn get_mut(&mut self, location: &[u32; 3]) -> Option<&mut V>{
        if let Some(index) = self.accelerator.get(location){
            self.buffer.get_mut(*index)
        }else{
            None
        }
    }

    pub fn get_voxel_by_index(&self, index: &VoxIndex) -> Option<&V>{
        self.buffer.get(*index)
    }
    
    pub fn get_voxel_by_index_mut(&mut self, index: &VoxIndex) -> Option<&mut V>{
        self.buffer.get_mut(*index)
    }

    pub fn get_accelerator(&self) -> &A{
        &self.accelerator
    }

    pub fn iter<'a>(&'a self) -> VoxelIterator<'a, V, A>{
        VoxelIterator{
            last_index: 0,
            data: self
        }
    }
}


impl<V,F> FlattenTree<V,F> for Object<V, SVO<V>> where V: ToFlatVoxel<V,F>, V: Tooling, V: Clone{
    fn to_flat_tree(&mut self) -> Vec<F>{
        //Index tree, then fire recursive algorithm of the private trait
        let mut index = 0;
        self.accelerator.get_root_mut().index_tree(&mut index);
        let mut flat_buffer = Vec::with_capacity(index+1); //last index mus be size of vec

        
        self.accelerator.get_root().to_flat(
            std::usize::MAX, //First skip index must be the max value
            0,
            &mut flat_buffer,
            &self.buffer
        );
        
        flat_buffer
    }
}

///Creates a simple object that is accelerated by a hashmap.
pub fn simple_object<V>() -> Object<V, hash_acc::HashAcc>{
    Object::new(hash_acc::HashAcc::new())
}

///Iterator over each voxel in an object. This does not contain non-leaf voxel when an tree like accelerator is used.
///
///Returns the location and the voxel in order of this loop:
/// ```
/// for x in 0..2{
///    for y in 0..2{
///        for z in 0..2{
///            let index = x*4 + y*2 + z;
///        }
///    }
/// }
/// 
///```
pub struct VoxelIterator<'a, V, A> where A: Accelerator<V>{
    last_index: usize,
    data: &'a Object<V,A>
}

impl<'a, V, A> Iterator for VoxelIterator<'a, V, A> where A: Accelerator<V> {
    // we will be counting with usize
    type Item = ([u32;3], &'a V);

    // next() is the only required method
    fn next(&mut self) -> Option<Self::Item> {
        let dimensions = self.data.get_accelerator().dimensions();
        //Search for next voxel in volume
        loop{
            let mut this_index = self.last_index;
            self.last_index += 1;

            //Check if index exceeds volume, return with none
            if this_index >= (dimensions[0] * dimensions[1] * dimensions[2]) as usize{
                return None;
            }
            
            let x: u32 = (this_index / (dimensions[1] as usize + dimensions[2] as usize)) as u32;
            this_index -= (x * (dimensions[1] * dimensions[2])) as usize; //find rest index
            let y: u32 = (this_index / dimensions[2] as usize) as u32;
            this_index -= (y * dimensions[2]) as usize;
            let z: u32 = this_index as u32;

            
            if let Some(d) = self.data.get(&[x,y,z]){
                return Some(([x,y,z], d));
            }
        }
    }
}

///When implemented on a voxel, in an object, the tooling function on the object can be used.
pub trait Tooling{
    ///Should interpolate a voxel with `other` voxels given a weight (which is between (0;1], and the first tuple
    /// element of the slice field).
    ///
    ///The function can be used to implement custom kernel operation on a volume.
    fn interpolate(&mut self, own_weight: f32, others: &[(f32, Self)]) where Self: std::marker::Sized ;
    ///Should return the "empty" representation used for a voxle.
    ///
    ///For instance, when the voxel only represents a alpha value in a cloud, the alpha value would be 0.
    fn empty() -> Self;
}

///Collection of useful tools implemented on any Voxel object where the voxel itself implements the `Tooling` trait.
pub trait Tools{
    fn blur(&mut self, is_gaussian: bool);
}

impl<V,A> Tools for Object<V, A> where V: Tooling, V: Sized, V: Clone, A: Accelerator<V>{
    ///Applies simple 3x3x3 gaussian blur to the object.
    fn blur(&mut self, is_gaussian: bool){
        let dimensions = self.accelerator.dimensions();
        //For every voxel in the buffer, get all sourrounding voxels
        for x in 0..dimensions[0]{
            for y in 0..dimensions[1]{
                for z in 0..dimensions[2]{
                    //gather all voxels and around this one and interpolate, then write back.
                    let mut num_present_voxels = 0;
                    let mut gathered_voxel = Vec::new();
                    for gx in -1..2{
                        for gy in -1..2{
                            for gz in -1..2{
                                //If we can't access the voxel since its out of bound, skipp
                                if (x as i64 + gx as i64) < 0 || (y as i64 + gy as i64) < 0 || (z as i64 + gz as i64) < 0{
                                    continue;
                                }
                                //If indexing into self, also ignore
                                if (gx == 0) && (gy == 0) && (gz == 0){
                                    continue;
                                } 

                                let ix = (x as i64 + gx as i64) as u32;
                                let iy = (y as i64 + gy as i64) as u32;
                                let iz = (z as i64 + gz as i64) as u32;

                                
                                let v = if let Some(v) = self.get(&[ix, iy, iz]){
                                    num_present_voxels += 1;
                                    v.clone() //TODO remove clone
                                }else{
                                    V::empty()
                                };

                                gathered_voxel.push(v);
                            }
                        }
                    }

                    //Check if there is a voxel at x,y,z if not we can skip, otherwise we create an empty one for blending later.
                    if self.get(&[x,y,z]).is_none(){
                        //Skip if no voxels where gathered around "self" and self is also none.
                        if num_present_voxels == 0{
                            continue;
                        }else{
                            //Create a new empty voxel and interpolate the gathered ones inside.
                            self.insert([x,y,z], V::empty());
                        }
                    }

                    let outer_voxel_weight = if is_gaussian{
                        0.5 / 26.0
                    }else{
                        1.0 / 27.0
                    };

                    let self_weight = if is_gaussian{
                        0.5
                    }else{
                        1.0 / 27.0
                    };
                    //Now interpolate self with the given voxels
                    let voxel_weight_pairs: Vec<(f32, V)> = gathered_voxel.into_iter().map(|v| (outer_voxel_weight, v)).collect();
                    self.get_mut(&[x,y,z]).expect("Could not get bluring voxel").interpolate(self_weight,  &voxel_weight_pairs);
                }
            }
        }
    }
}

///A trait that can be implemented on custom accelerators to accelerate
/// access of voxels in a volume. This only operates on the voxel indices of an
/// associated `VoxelBuffer`. However, some acceleration structures are free to add and remove voxels from the buffer. For instance
/// the SVO accelerator adds intermediate voxels for non-leaf nodes which contain the interpolated content of all its children.
pub trait Accelerator<V>{
    ///Inserts a voxel at the given location. If there is already one present,
    /// the old voxel is discarded.
    fn insert(&mut self, location: [u32;3], voxel: VoxIndex, buffer: &mut VoxelBuffer<V>);
    ///Removes the given voxel from the Accelerator.
    fn delete(&mut self, location: &[u32;3], buffer: &mut VoxelBuffer<V>);
    fn get(&self, location: &[u32; 3]) -> Option<&VoxIndex>;
    fn get_mut(&mut self, location: &[u32; 3]) -> Option<&mut VoxIndex>;
    ///Returns the dimension of the accelerated object. This can change when a pixel outside of the current
    ///Bounds is pushed.
    fn dimensions(&self) -> [u32;3];
    ///Hints how many voxels are in the structure. Useful when allocating something for voxels.
    fn num_voxel(&self) -> usize;
}

