use crate::{
    VoxIndex,
    VoxelBuffer,
    Accelerator,
    Tooling
};

use std::marker::PhantomData;


///The node that makes one level of an octree.
///Can be used to implement recursive algorithms over the tree, but only by reading.
///Writing is currently not permitted since this can make the SVO inconsistent when done wrong.
///That might change later.
///
/// The root node of a tree is retrieved by calling `get_root()` on an `SVO`.
pub struct OctNode<V>{
    children: [Option<Box<OctNode<V>>>; 8],
    voxel_type: PhantomData<V>,
    //The Voxel this node references
    voxel: VoxIndex,
    //Minimum location of this voxel
    min: [u32; 3],
    //Maximum location
    max: [u32;3],

    //Might contain a correct index when the tree is flattend
    //into a depth first search array.
    dfs_index: usize,

    is_leaf: bool,
}

impl<V> OctNode<V> where V: Tooling, V: Clone{
    
    pub fn is_leaf(&self) -> bool{
        self.is_leaf
    }

    pub fn has_children(&self) -> bool{
        match self.children{
            [None, None, None, None, None, None, None, None] => false,
            _ => true,
        }
    }

    ///Gives out dfs_indices to all nodes
    pub fn index_tree(&mut self, index: &mut usize){
        self.dfs_index = *index;
        *index += 1;
        
        for x in 0..2{
            for y in 0..2{
                for z in 0..2{
                    if let Some(c) = &mut self.children[x*4 +y*2 +z]{
                        c.index_tree(index);
                    }
                }
            }
        }
    }
    
    ///Returns the extent in voxel for the node in voxels. So a leaf has always an extent of 1, the
    /// root has an extent of `resolution` and all children have some power of two resolution.
    pub fn extent(&self) -> [u32; 3]{
        extent(&self.min, &self.max)
    }

    ///Returns the min and max extent of this voxel within the structure.
    pub fn get_min_max(&self) -> ([u32;3], [u32;3]){
        (self.min, self.max)
    }

    ///Returns a reference to all the children of this voxel. If a child is `None`
    ///no child is attached to this node. If you want to index into the slice, the order from back to forth is like this:
    /// ```
    /// for x in 0..2{
    ///    for y in 0..2{
    ///        for z in 0..2{
    ///            let index = x*4 + y*2 + z;
    ///        }
    ///    }
    /// }
    /// 
    ///```
    pub fn children(&self) -> &[Option<Box<OctNode<V>>>; 8]{
        &self.children
    }

    ///Returns the voxel index into the associated `VoxelBuffer`. Be sure to use the correct one
    ///otherwise there will be undefined behavior.
    pub fn get_voxel_index(&self) -> &VoxIndex{
        &self.voxel
    }
    
    fn reatach_old_root(&mut self, old_root: Box<OctNode<V>>, buffer: &mut VoxelBuffer<V>){
        //Check if children are right size to be reatached (always at [0,0,0]).
        if half_extent(&self.min, &self.max)[0] == old_root.extent()[0]{
            debug_assert!(self.min[0] == 0 && self.min[1] == 0 && self.min[2] == 0, "Old root location does not match!");            
            self.children[0] = Some(old_root);
        }else{
            let (cmin, cmax) = get_node_mins_maxs(&self.min, &self.max, 0);
            //Insert new level into octree to be build upon
            let voxel_idx = buffer.push(V::empty());
            let mut new_level = Box::new(
                OctNode{
                    children: [None, None, None, None, None, None, None, None],
                    voxel_type: PhantomData,
                    voxel: voxel_idx,
                    min: cmin,
                    max: cmax,
                    is_leaf: false,
                    dfs_index: std::usize::MAX,
                }
            );

            new_level.reatach_old_root(old_root, buffer);
            self.children[0] = Some(new_level);
        }

        self.update_interpolation(buffer);
    }
    
    fn insert(&mut self, location: [u32; 3], voxel: VoxIndex, buffer: &mut VoxelBuffer<V>){
        
        let index = index_from_position(&self.min, &self.max, &location);
        //Check if self is the last node before leafs. If so, add as child to index
        if self.extent()[0] == 2{
            let new_node = Box::new(
                OctNode{
                    children: [None, None, None, None, None, None, None, None],
                    voxel_type: PhantomData,
                    voxel: voxel,
                    min: location,
                    max: [
                        location[0] + 1,
                        location[1] + 1,
                        location[2] + 1,
                    ],
                    is_leaf: true,
                    dfs_index: std::usize::MAX,
                }
            );            
            self.children[index] = Some(new_node);
        }else{

            //Check if there is already a child at the index, if so, pass down, else create new child
            // and traverse
            if let Some(child) = &mut self.children[index]{
                child.insert(location, voxel, buffer);
            }else{
                //Have to create a sub child and try again to insert
                let (min, max) = get_node_mins_maxs(&self.min, &self.max, index);
                let voxel_idx = buffer.push(V::empty());
                let mut new_child = Box::new(
                    OctNode{
                        children: [None, None, None, None, None, None, None, None],
                        voxel_type: PhantomData,
                        voxel: voxel_idx,
                        min,
                        max,
                        is_leaf: false,
                        dfs_index: std::usize::MAX,
                    }
                );
                //Add node to new child, then add child to parent.
                new_child.insert(location, voxel, buffer);
                self.children[index] = Some(new_child);
            }
        }
        
        self.update_interpolation(buffer);
    }

    ///Searches for leaf at `location` and if found deletes it. Returns false when
    ///all children of this voxel are `None` in which case this node can be removed from its parent.
    fn delete(&mut self, location: &[u32;3], buffer: &mut VoxelBuffer<V>) -> bool{
        let index = index_from_position(&self.min, &self.max, location);

        if let Some(child) = &mut self.children[index]{
            //If there is a child and it is a leaf mark for deletion.
            if child.is_leaf(){
                buffer.delete(child.voxel);
                self.children[index] = None;
            }else{
                //Search for leaf in this child and delete. If false is returned,
                //this child has no children anymore, therefore it can be deleted as well.
                if !child.delete(location, buffer){
                    //Since this child has no children anymore, mark for deletion as well and
                    //return
                    buffer.delete(child.voxel);
                    self.children[index] = None;
                    return self.has_children();
                }
            }
        }else{
            //No such child, therefore we can return
            return self.has_children();
        }
        //Anyways update interpolation
        self.update_interpolation(buffer);
        self.has_children()
    }
    
    fn get(&self, location: &[u32; 3]) -> Option<&VoxIndex>{
        let index = index_from_position(&self.min, &self.max, location);
        
        if let Some(ref node) = self.children[index]{
            if node.is_leaf(){
                return Some(&node.voxel)
            }else{
                node.get(location)
            }
        }else{
            None
        }
    }
    
    fn get_mut(&mut self, location: &[u32; 3]) -> Option<&mut VoxIndex>{

        let index = index_from_position(&self.min, &self.max, location);
        if let Some(ref mut node) = self.children[index]{
            if node.is_leaf(){
                return Some(&mut node.voxel)
            }else{
                node.get_mut(location)
            }
        }else{
            None
        }        
    }

    fn update_interpolation(&mut self, buffer: &mut VoxelBuffer<V>){
        //Should not interpolate since it is a leaf.
        if self.is_leaf(){
            return;
        }
        
        let children: Vec<_> = self.children.iter().filter_map(|c| if let Some(sub) = c {Some(sub)}else{None}).collect();
        let num_children = children.len() as f32;
        let weight_children_pair: Vec<(f32, V)> = children.into_iter().map(|v|{
            (
                1.0 / num_children,
                if let Some(innerv) = buffer.get(v.voxel){
                    innerv.clone()
                }else{
                    debug_assert!(false, "Could not get voxel that is structure!");
                    V::empty()
                }
            )
        }).collect();
        if let Some(voxel_data) = buffer.get_mut(self.voxel){
            voxel_data.interpolate(0.0, &weight_children_pair);
        }else{
            debug_assert!(false, "Could not find voxel data for voxel in octree while interpolating!");
        }
    }

    fn print_nodes(&self){
        for i in 0..8{
            if let Some(c) = &self.children[i]{
                if c.is_leaf(){
                    println!("\t node at: {:?} with extent: {:?}", c.min, c.extent());
                }else{
                    c.print_nodes();
                }
            }
        }
    }
}

///Implemented on all SVOs that have voxels which implement the `ToFlatvoxel` trait.
///Takes a tree and flattens it into a DepthFirst-Ordered vector. The `skip_index` that is passed
///to the `flatten` function points to the index in the vector which can be skipped to while traversal when the next child
///is not usable.
///
///The structure is build like this, when the nodes in the tree contain the index at which they are stored into `buffer, and the  `skip_index` as second value:
/// ```markdown
///             0,MAX
///          /        \
///         1,6       6,MAX
///        / \        /  \
///      2,5  5,6   7,8  8,MAX
///      / \
///    3,4  4,5
///```
pub trait FlattenTree<V,F>{
    fn to_flat_tree(&mut self) -> Vec<F>;
}



pub trait FlattenNode<V, F> where V: ToFlatVoxel<V,F>{
    fn to_flat(&self, skip_index: usize, level: usize, buffer: &mut Vec<F>, voxel_buffer: &VoxelBuffer<V>);
}

impl<V,F> FlattenNode<V,F> for OctNode<V> where V: ToFlatVoxel<V,F>, V: Tooling, V: Clone{
    fn to_flat(&self, skip_index: usize, level: usize, buffer: &mut Vec<F>, voxel_buffer: &VoxelBuffer<V>) {
        /*
        let own = object
            .get_voxel_by_index(self.get_voxel_index())
            .expect("Could not get voxel for index!")
            .to_gpu_voxel(skip_index);

        */
        let flat_node = voxel_buffer
            .get(*self.get_voxel_index())
            .expect("Could not get voxel for index!")
            .flatten(
                skip_index,
                level,
                &self
            );
        
        
        buffer.push(flat_node);
        //Find the dfs index we gave to this node and check that we are inserting at the right location.
        assert!(
            buffer.len() == (self.dfs_index + 1),
            "Pushed voxel to wrong index: VoxelIdx: {}, buffer_len: {}",
            self.dfs_index,
            buffer.len()
        );

        for x in 0..2{
            for y in 0..2{
                for z in 0..2{
                    if let Some(ref c) = self.children()[x*4 + y*2 + z]{
                        //The new skip index will be the next possible child, or, if there is non,
                        //the parents skip index.
                        let new_skip_index = {
                            let mut next_pos_child = x*4 + y*2 + z + 1;
                            let mut next_skip_index = None;

                            while next_pos_child < 8{
                                if let Some(ref c) = self.children()[next_pos_child]{
                                    next_skip_index = Some(
                                        c.dfs_index
                                    );
                                    break;
                                }
                                //Change to the next one
                                next_pos_child+=1;
                            }

                            if let Some(sk_idx) = next_skip_index{
                                sk_idx
                            }else{
                                //If we could not find a child with a skip index, use the parent
                                skip_index
                            }
                        };

                        
                        c.to_flat(new_skip_index, level + 1, buffer, voxel_buffer);
                    }
                }
            }
        }
    }
}

///Standard implementation of a SparseVoxelOctree.
pub struct SVO<V> where V: Tooling{
    root: Box<OctNode<V>>,
    num_voxel: usize,
}

impl<V> SVO<V> where V: Tooling, V: Clone{
    ///Create the SVO for an optional resolution.
    ///The resolution is optional. If none is given 1x1 is used.
    /// The volume grows when a not contained voxel is added. However, this might take
    /// longer compared to inserting into a already correctly sized volume.
    pub fn new(resolution: Option<u32>) -> Self{
        SVO{
            root: Box::new(OctNode{
                children: [None, None, None, None, None, None, None, None],
                voxel_type: PhantomData,
                voxel: 0,
                min: [0; 3],
                max: if let Some(res) = resolution{
                    [res; 3]
                }else{
                    [1; 3]
                },
                is_leaf: false,
                dfs_index: std::usize::MAX,
            }),
            num_voxel: 1 
        }
    }

    pub fn print_all_nodes(&self){
        self.root.print_nodes();
    }
    
    ///Returns a reference to the root node, can be used to implement recursive
    ///algorithms over the tree by calling the `children()` function on every node.
    pub fn get_root(&self) -> &Box<OctNode<V>>{
        &self.root
    }

    pub fn get_root_mut(&mut self) -> &mut Box<OctNode<V>>{
        &mut self.root
    }
}



///Implementation for every voxel that implements the `Tooling` trait.
///
///The trait is needed for interpolated parent structures to be calculated.
impl<V> Accelerator<V> for SVO<V> where V: Tooling, V: Clone{
    fn insert(&mut self, location: [u32;3], voxel: VoxIndex, buffer: &mut VoxelBuffer<V>){
        //First check if the added voxel is within the structures bounds.
        //If not, be have to extent the structure by multiplying each axis by two
        // and adding a new top level node.
        if !point_intersects(&self.root.min, &self.root.max, &location){
            //Apparently the volume has to grow to fit the new voxel.
            //Therefore grow until we found a big enough root voxel
            //then insert the old root into it.

            //Note: For the SVO the roots max value is always the resolution. and min
            //is always 0.
            let mut new_resolution = self.root.max[0];
            while !point_intersects(&[0;3], &[new_resolution; 3], &location){
                new_resolution *= 2;
            }

            let mut new_root_node = Box::new(OctNode{
                children: [None, None, None, None, None, None, None, None],
                voxel_type: PhantomData,
                voxel: 0,
                min: [0; 3],
                max: [new_resolution; 3],
                is_leaf: false,
                dfs_index: std::usize::MAX,
            });
            //Exchange old and new, then add the old root as node to the new root.
            std::mem::swap(&mut new_root_node, &mut self.root);
            self.root.reatach_old_root(new_root_node, buffer);
            //the "new_root" is now the old one.
            //self.root.insert_node(new_root_node);
        }
        //Possible growing is handled, now insert node
        self.root.insert(location, voxel, buffer);

        self.num_voxel += 1;
    }
    fn delete(&mut self, location: &[u32;3], buffer: &mut VoxelBuffer<V>){
        self.root.delete(location, buffer);
        self.num_voxel -= 1;
    }
    fn get(&self, location: &[u32; 3]) -> Option<&VoxIndex>{
        self.root.get(location)
    }
    fn get_mut(&mut self, location: &[u32; 3]) -> Option<&mut VoxIndex>{
        self.root.get_mut(location)
    }
    fn dimensions(&self) -> [u32;3]{
        self.root.max
    }
    fn num_voxel(&self) -> usize {
        self.num_voxel
    }
}


///A trait, which when implemented should be able to convert a voxel into its flatten ed representation.
///When a structure is flattned the hierachy is given by the order of the vector in which the voxels
///are contained and an optional `skip_index` which points to the next voxel on the same level. For more information about
///the whole structure setup, have a look at the docs of the `Flatten` trait.
pub trait ToFlatVoxel<V,F>{
    fn flatten(&self, skip_index: usize, level: usize, node: &OctNode<V>) -> F;
}

#[inline]
fn point_intersects(min: &[u32; 3], max: &[u32; 3], point: &[u32; 3]) -> bool{
    if min[0] > point[0] || max[0] <= point[0] ||
        min[1] > point[1] || max[1] <= point[1] ||
        min[2] > point[2] || max[2] <= point[2]{
            false
        }else{
            true
        }
}

#[inline]
fn extent(min: &[u32; 3], max: &[u32; 3]) -> [u32;3]{
    [
        (max[0] - min[0]),
        (max[1] - min[1]),
        (max[2] - min[2]),
    ]
}

#[inline]
fn half_extent(min: &[u32; 3], max: &[u32; 3]) -> [u32;3]{
    [
        (max[0] - min[0]) / 2,
        (max[1] - min[1]) / 2,
        (max[2] - min[2]) / 2,
    ]
}

#[inline]
fn index_from_position(min: &[u32; 3], max: &[u32; 3], location: &[u32; 3]) -> usize{

    assert!(point_intersects(min, max, location), "Point not in box, cannot calc index!");
    
    let extent_half = half_extent(min, max);

    let x = if (min[0] + extent_half[0]) <= location[0]{
        1
    }else{
        0
    };

    let y = if (min[1] + extent_half[1]) <= location[1]{
        1
    }else{
        0
    };

    let z = if (min[2] + extent_half[2]) <= location[2]{
        1
    }else{
        0
    };

    (x*4) + (y*2) + z
}

#[inline]
fn index_to_xyz(index: usize) -> (u32, u32, u32){
    assert!(index < 8, "index_to_xyz: index must be < 8, but was: {}", index);
    match index{
        0 => (0,0,0),
        1 => (0,0,1),
        2 => (0,1,0),
        3 => (0,1,1),
        4 => (1,0,0),
        5 => (1,0,1),
        6 => (1,1,0),
        7 => (1,1,1),
        _ => panic!("Could not get index to xyz"),
    }
}

///Returns the mins of maxs of a child, given its parent mins/maxs as well as the index (0<=index<8).
///Used when inserting new children and searching for the correct child to add to.
fn get_node_mins_maxs(parent_min: &[u32;3], parent_max: &[u32; 3], index: usize) -> ([u32;3], [u32;3]){
    assert!(index < 8, "child index must be smaller then 8, was {}", index);

    let (x,y,z) = index_to_xyz(index);
    //Note: its save to do the integer division here, since the extent always must be a multiple of 8.
    let extent_half = half_extent(parent_min, parent_max);

    (
        [
            parent_min[0] + (x * extent_half[0]),
            parent_min[1] + (y * extent_half[1]),
            parent_min[2] + (z * extent_half[2])
        ],
        [
            parent_min[0] + ((x+1) * extent_half[0]),
            parent_min[1] + ((y+1) * extent_half[1]),
            parent_min[2] + ((z+1) * extent_half[2])
        ]
    )
}
