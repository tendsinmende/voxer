use std::collections::HashMap;
use crate::{
    VoxIndex,
    Accelerator,
    VoxelBuffer
};
///A standard hashmap accelerator that maps a location to an index.
pub struct HashAcc{
    map: HashMap<[u32;3], VoxIndex>,
    current_dimensions: [u32;3],
        
}

impl HashAcc{
    pub fn new() -> Self{
        HashAcc{
            map: HashMap::new(),
            current_dimensions: [0;3],
        }
    }
}

impl<V> Accelerator<V> for HashAcc{
    fn insert(&mut self, location: [u32;3], voxel: VoxIndex, _buffer: &mut VoxelBuffer<V>){
        self.current_dimensions[0] = self.current_dimensions[0].max(location[0]);
        self.current_dimensions[1] = self.current_dimensions[1].max(location[1]);
        self.current_dimensions[2] = self.current_dimensions[2].max(location[2]);

        self.map.insert(location, voxel);
    }
    
    fn delete(&mut self, location: &[u32; 3], _buffer: &mut VoxelBuffer<V>){
        self.map.remove(location);
    }
    fn get(&self, location: &[u32; 3]) -> Option<&VoxIndex>{
        self.map.get(location)
    }
    fn get_mut(&mut self, location: &[u32; 3]) -> Option<&mut VoxIndex>{
        self.map.get_mut(location)
    }
    fn dimensions(&self) -> [u32;3]{
        self.current_dimensions
    }
    fn num_voxel(&self) -> usize {
        self.map.len()
    }
}
