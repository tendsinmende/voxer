use crate::*;

///Can be implemented on a combination of `V` and `A` for custom Voxel-Formats
pub trait Decode<V, A: Accelerator>{
    pub fn decode(file: std::io::File) -> Object<V, A>;
}
