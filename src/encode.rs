
pub enum EncodeError{
    IoError(std::io::Error)
}

///When implemented on some type, it is possible to write an object
///to file.
pub trait Encode{
    fn encode(&self, file: std::io::File) -> Result<(), EncodeError>
}
