# Voxer

A Rust crate to work with voxel objects similar to [image](https://crates.io/crates/image) but in 3D.

# Basic structure:
The library is able to load and store voxel volumes from files via the `Encode` and `Decode` traits.
Those objects are then stored in a linear `VoxelBuffer`. The buffer itself only stores the voxels but has no knowledge 
where in the volume the voxels are located. This is handled by the associated `Accelerator`.

The library is Voxel-agnostic. You can store anything as a voxel. However, if you want some further functionality like
bluring the volume, the stored object needs to implement the `Tooling` trait.

An Accelerator handles indexing the buffer when trying to receive a voxel at a location. The two most common types are
a HashMap (implemented as `HashAcc`) and an Sparse-Voxel-Octree (implemented as `SVO`). But you can also implement
new accelerators via the `Accelerator` trait.

# Documentation
Clone the repository then execute the following in the repo to get a version opened in your browser.

``` 
cargo doc --open
```

# Usage
Creating a simple object and inserting a voxel:
```
use voxer::{
    Object,
    octree::SVO
};

struct Voxel(f32);
fn main(){
    let mut object: Object<Voxel, _> = Object::new(Box::new(SVO::new(None)));
    object.insert([32,56,7], Voxel(25.0));
}
```

# License
The whole library is licesend under MPL 2.0. Every contribution will be licensed under it as well.

For more information see the `LICENSE` file in the repository


